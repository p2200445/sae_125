**Contexte**

Ce projet a pour objectif de créer une IHM pour un responsable des dispensaires d’un district avec au minimum 30 dispensaires, dans un pays sans infrastructure routière développée, où
chaque trajet comporte des risques ( piste impraticable, pont
infranchissable, vols de médicaments sur le chemin, attaques de
gangs armés,...). L'objectif de ce projet est de faciliter et optimiser les déplacements routiers dans un secteur difficile d'accès.

**Spécifications** :

\- L’IHM devra pouvoir indiquer un chemin en prenant en compte
différents paramètres (indiquer le chemin le plus rapide, le
plus court en distance, ou le plus fiable et en donnant entre 0
et 2 sommets du graphe).

**Liste des Fonctionnalitées de ce projet**

- F1. Charger un graphe.
- F2. Afficher un graphe.
- F3. Modifier les caractéristiques du graphe par fichier.
- F4. Modifier les caractéristiques du graphe par saisie à l’écran.
- F5. Lister tous les nœuds (regroupés par catégories) ou un type donné de nœud, et leur nombre par type.
- F6. Lister toutes les arêtes et donner leur nombre.
- F7. Pour un sommet donné, lister les voisins directs (nœuds à 1-distance).
- F8. Pour une arête donnée, lister les sommets qu’elle relie.
- F9. Pour un sommet donné, lister les voisins directs d’un type donné (nœuds à 1-distance).
- F10. Pour 2 sommets donnés, lister les sommets voisins d’un type donné des centres S1 et S2(ex. : liste des blocs opératoires en voisins directs de S1 et S2).
- F11. Étant donné 2 nœuds, dire s’ils sont à 2-distance ou pas.
- F12. Comparer 2 villes, sur le critère Opératoire (nb de Blocsopératoires à plus de 2-distance), le critère Maternité (nb de maternités à plus de 2-distance) ou Nutritionnel (nb de centres de nutrition à plus de 2-distance).
- F13. Étant donné 2 nœuds quelconques du graphe, définir le chemin le plus court.
- F14. Afficher la complexité de votre algorithme du chemin le plus fiable.
- F15. Donner le chemin le plus court en distance et en durée entre 2 sites.
- F16. Saisir deux points sur le graphe pour F11-F15.
- F17. Pouvoir modifier visuellement le graphe(déplacer des noeuds).
- F18. Afficher visuellement le chemin le plus fiable, le plus court en distance ou en durée.
- F19. Trouver une route traversant au moins un dispensaire de chaque catégorie (nutrition, bloc, maternité).
- F20. Trouver une route traversant exactement un nombre donné par l’utilisateur, de dispensaire de chaque catégorie (ex. : 4 centres de nutrition, 1 bloc, 3 maternités).