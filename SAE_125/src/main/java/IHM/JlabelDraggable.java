package IHM;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class JlabelDraggable extends JLabel implements MouseMotionListener {
    public JlabelDraggable(String text) {
        super.setText(text);
        setHorizontalAlignment(SwingConstants.CENTER);
        setVerticalAlignment(SwingConstants.CENTER);
        addMouseMotionListener(this);
    }
    public void mouseDragged(MouseEvent e){
		setBounds(this.getX()+e.getX()-this.getWidth()/2,this.getY()+e.getY()-this.getHeight()/2, 50, 50);
    }
	public void mouseMoved(MouseEvent e){}
}