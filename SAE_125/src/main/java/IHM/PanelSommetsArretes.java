package IHM;

import CSV.ListeSommets;
import CSV.ListeTrajets;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.util.Comparator;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class PanelSommetsArretes extends JPanel {
    public PanelSommetsArretes(ListeTrajets trajets, ListeSommets sommets){
        // Création du Panel listant les sommets et arrêtes

            // Liste des sommets
        JLabel titreSommets = new JLabel("Liste des sommets");
        String[]nomColonnesSomm = {"Nom", "Type", "Sommets adjacents"};
        Object[][]dataSommet = sommets.sommetsTab();
        DefaultTableModel tabModelSom = new DefaultTableModel(dataSommet, nomColonnesSomm) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JTable tabSommet = new JTable(tabModelSom);
        TableRowSorter<DefaultTableModel> sorterSom = new TableRowSorter<>(tabModelSom);
        tabSommet.setRowSorter(sorterSom);
        int widthSom = tabSommet.getPreferredSize().width;
        int heightSom = tabSommet.getPreferredSize().height;
        tabSommet.setPreferredScrollableViewportSize(new Dimension(widthSom, heightSom/2));
        tabSommet.setFillsViewportHeight(true);
        JScrollPane scrollPaneSommets = new JScrollPane(tabSommet);
        scrollPaneSommets.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPaneSommets.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

            // Liste des arrêtes
        JLabel titreArretes = new JLabel("Liste des arrêtes");
        String[]nomColonnesTraj = {"N°", "Fiabilité(%)", "Distance(km)", "Durée(min)", "Sommets reliés"};
        Object[][] dataTraj = trajets.trajetsTab();
        DefaultTableModel tabModelTraj = new DefaultTableModel(dataTraj, nomColonnesTraj) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Make all cells non-editable
            }
        };
        JTable tabTrajet = new JTable(tabModelTraj);
        TableRowSorter<DefaultTableModel> sorterTraj = new TableRowSorter<>(tabModelTraj);
        tabTrajet.setRowSorter(sorterTraj);
        tabTrajet.setPreferredScrollableViewportSize(new Dimension(widthSom*2, heightSom/2));
        tabTrajet.setFillsViewportHeight(true);
        JScrollPane scrollPaneArretes = new JScrollPane(tabTrajet);
        scrollPaneArretes.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPaneArretes.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        Comparator<Object> customComparator = (o1, o2) -> {
            if (o1 instanceof String && o2 instanceof String) {
                return ((String) o1).compareToIgnoreCase((String) o2);
            } else if (o1 instanceof Integer && o2 instanceof Integer) {
                return Integer.compare((Integer) o1, (Integer) o2);
            } else if (o1 instanceof Double && o2 instanceof  Double){
                return Double.compare((Double) o1, (Double) o2);
            }
            return 0;
        };
        sorterTraj.setComparator(1, customComparator);
        sorterTraj.setComparator(2, customComparator);
        sorterTraj.setComparator(3, customComparator);
        sorterTraj.setComparator(4, customComparator);

        // Nombres de dispensaires de chaque type :
        JPanel nbrDispType = new JPanel();
        nbrDispType.setLayout(new BoxLayout(nbrDispType,BoxLayout.Y_AXIS));
        JLabel mat = new JLabel("Nombre de maternités : "+ListeSommets.getNbrM());
        JLabel nut = new JLabel("Nombre de centre de nutritions : "+ListeSommets.getNbrN());
        JLabel bloc = new JLabel("Nombre de blocs opératoires : "+ListeSommets.getNbrO());

        nbrDispType.add(mat);
        nbrDispType.add(nut);
        nbrDispType.add(bloc);

        JPanel sommetsTrajets = new JPanel();
        sommetsTrajets.setLayout(new BoxLayout(sommetsTrajets,BoxLayout.Y_AXIS));
        sommetsTrajets.add(titreSommets);
        sommetsTrajets.add(scrollPaneSommets);
        sommetsTrajets.add(titreArretes);
        sommetsTrajets.add(scrollPaneArretes);
        sommetsTrajets.add(nbrDispType);
        add(sommetsTrajets);
    }
}