package IHM;

import CSV.*;
import Exceptions.ExceptionDispencereExistePas;
import Exceptions.ExceptionTousNul;
import Exceptions.ExceptionTrajetExistePas;
import Exceptions.ExceptionTypeNegatif;
import GrapheMaillons.LCGraphe;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;

public class GrapheIHM extends JFrame {
    private final LCGraphe lcGraphe;
    private Map<Integer, JlabelDraggable> hashMapSommets;
    private PanelGraphe panelGraph;
    private ListeTrajets trajets;
    private ListeSommets sommets;
    private JButton choixFichierButton;
    private JButton modifSommetButton;
    private JButton modifTrajetButton;
    private JButton f10;
    private JButton f11;
    private JButton f12;
    private JButton f13;
    private JButton f15;
    private JButton f19;
    private JButton f20;

    public GrapheIHM(ListeSommets sommetsList, ListeTrajets trajetsList){
        super("TestGraphe");
        this.trajets = trajetsList;
        this.sommets = sommetsList;
        this.lcGraphe = ChargementGraphe.loadingLCGraph(this.sommets, this.trajets);
        initComponents();
        initEventListeners();
        setPreferredSize(new Dimension(1350, 700)); // Taille fenêtre
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public void initComponents(){
    // Ajout des sommets dans une hashMap
        this.hashMapSommets = new HashMap<>();
        for (int i = 0; i < this.sommets.getSize(); i++){
            Sommet temp = this.sommets.getSommetbyID(i+1);
            JlabelDraggable jlabelDraggable = new JlabelDraggable("S"+temp.getNumSommet());
            jlabelDraggable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 2){
                        JOptionPane.showMessageDialog(null, temp.toString());
                    }
                }
            });
            hashMapSommets.put(temp.getNumSommet(), jlabelDraggable);
        }

    // Création du Graphe, se référer à la classe PanelGraph pour plus d'informations
        this.panelGraph = new PanelGraphe(sommets, trajets, hashMapSommets);
        panelGraph.setBackground(Color.WHITE); // Modif Couleur fond graphe

    // Création du Panel Listant les sommets et arrêtes, se référer à la classe PanelList pour plus d'infos
        PanelSommetsArretes sommetsTrajets = new PanelSommetsArretes(this.trajets, this.sommets);

    // CheckBox Pour choisir valeur affichée sur trajets
        JPanel checkBoxPanel = new JPanel();
        JLabel checkBoxTitle = new JLabel("Choisissez la valeur affichée sur chaque sommet");
        JRadioButton checkBoxNum = new JRadioButton("Numéro des trajets");
        checkBoxNum.setSelected(true);
        JRadioButton checkBoxFiab = new JRadioButton("Fiabilité des trajets(%)");
        JRadioButton checkBoxDist = new JRadioButton("Distance des trajets(km)");
        JRadioButton checkBoxDur = new JRadioButton("Durée des trajets(min)");
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(checkBoxNum);
        buttonGroup.add(checkBoxFiab);
        buttonGroup.add(checkBoxDist);
        buttonGroup.add(checkBoxDur);
        ActionListener actionListener = e -> {
            JRadioButton source = (JRadioButton) e.getSource();
            if (source.isSelected()){
                switch (source.getText()) {
                    case "Numéro des trajets" -> panelGraph.resetState();
                    case "Fiabilité des trajets(%)" -> panelGraph.updateState("Fiab");
                    case "Distance des trajets(km)" -> panelGraph.updateState("Dist");
                    case "Durée des trajets(min)" -> panelGraph.updateState("Dur");
                }
            }
        };
        checkBoxNum.addActionListener(actionListener);
        checkBoxFiab.addActionListener(actionListener);
        checkBoxDist.addActionListener(actionListener);
        checkBoxDur.addActionListener(actionListener);
        checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.X_AXIS));
        checkBoxPanel.add(checkBoxTitle);
        checkBoxPanel.add(checkBoxNum);
        checkBoxPanel.add(checkBoxFiab);
        checkBoxPanel.add(checkBoxDist);
        checkBoxPanel.add(checkBoxDur);

    // Boutons pour exécuter les fonctionnalités du graphe
        this.choixFichierButton = new JButton("<html>Changer Fichiers</html>");
        this.modifSommetButton = new JButton("<html>Modifier sommet</html>");
        this.modifTrajetButton = new JButton("<html>Modifier trajet</html>");
        this.f10 = new JButton("<html> Liste des Sommets <br> d'un type donné <br> à 1-distance <br> de 2 sommets donnés</html>");
        this.f11 = new JButton("<html> Voisins à 2-Distance <br> d'un sommet</html>");
        this.f12 = new JButton("<html> Comparaison de villes </html>");
        this.f13 = new JButton("<html> Chemin le plus court <br> entre 2 sommets</html>");
        this.f15 = new JButton("<html> Chemin le plus fiable <br> et le plus rapide <br> entre 2 sommets</html>");
        this.f19 = new JButton("<html> Route traversant <br> un dispensaire <br> de chaque type</html>");
        this.f20 = new JButton("<html> Route traversant <br> un nombre donné <br> de chaque type <br> de dispensaire</html>");
    // Titre des fonctionnalités du graphe
        JLabel titreFonct = new JLabel("Fonctionnalités");

    // Création du JPanel des fonctionnalités et implémentation des boutons et du titre à l'intérieur
        JPanel fonctions = new JPanel();
        fonctions.setLayout(new BoxLayout(fonctions, BoxLayout.Y_AXIS));
        fonctions.add(titreFonct);
        fonctions.add(choixFichierButton);
        fonctions.add(modifSommetButton);
        fonctions.add(modifTrajetButton);
        fonctions.add(f10);
        fonctions.add(f11);
        fonctions.add(f12);
        fonctions.add(f13);
        fonctions.add(f15);
        fonctions.add(f19);
        fonctions.add(f20);

    // Ajout de tout les JPanel dans le JFrame
        Container container = getContentPane();
        container.add(checkBoxPanel, BorderLayout.SOUTH);
        container.add(panelGraph, BorderLayout.CENTER);
        container.add(sommetsTrajets, BorderLayout.WEST);
        container.add(fonctions, BorderLayout.EAST);
    }
    public static File choixFichier(){
        File file = null;
        JFileChooser choose = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        int res = choose.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
        file = choose.getSelectedFile();
    }
        return file;
    } // Fonction qui demande un fichier à l'utilisateur
    public Sommet choixSommet() throws ExceptionDispencereExistePas{
        String inputNum = JOptionPane.showInputDialog(null, "Nom dispensaire ?");
        if (this.sommets.getSommetbyString(inputNum) == null){
            throw new ExceptionDispencereExistePas(inputNum);
        } else {
            return this.sommets.getSommetbyString(inputNum);
        }
    } // Fonction qui demande un sommet à l'utilisateur
    public Trajet choixTrajet() throws ExceptionTrajetExistePas {
        String inputNum = JOptionPane.showInputDialog(null, "Numéro trajet ?");
        try {
            if (this.trajets.getTraj(Integer.parseInt(inputNum)) == null) {
                throw new ExceptionTrajetExistePas(inputNum);
            } else {
                return trajets.getTraj(Integer.parseInt(inputNum) - 1);
            }
        } catch (NumberFormatException e){
            throw new ExceptionTrajetExistePas(inputNum);
        }
    } // Fonction qui demande un trajet à l'utilisateur
    public String choixType(){
        String[] optionType = {"Bloc opératoire", "Maternité", "Centre de nutrition"};
        int selectedOption = JOptionPane.showOptionDialog(null, "Type dispensaire ?","Choix type", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionType, optionType[0]);
        String type = null;
        switch (selectedOption){
            case 0 -> type = "O";
            case 1 -> type = "M";
            case 2 -> type = "N";
        }
        return type;
    } // Fonction qui demande un type à l'utilisateur
    // Setters
    public void setSommets(ListeSommets sommets) {
        this.sommets = sommets;
    }
    public void setTrajets(ListeTrajets trajets) {
        this.trajets = trajets;
    }
    public void initEventListeners() {
        this.choixFichierButton.addActionListener(e -> {
            ListeSommets listeSommets = new ListeSommets();
            ListeTrajets listeTrajets = new ListeTrajets();
            File f1 = choixFichier(); // Demande le premier fichier
            File f2 = choixFichier(); // Demande le second fichier
            ChargementGraphe.loadingGraph(f1, f2, listeSommets, listeTrajets); // Se réferer à la classe chargementGraphe
            listeTrajets.supprDoublons(); // Supprime les doublons de la liste de trajets
            setSommets(listeSommets); // Met à jour les nouvelles valeurs de la liste de sommets
            setTrajets(listeTrajets); // Met à jour les nouvelles valeurs de la liste de trajets
            dispose(); // Ferme la page
            fastMain(this.sommets, this.trajets); // Ouvre une nouvelle page avec les nouvelles valeurs
        });
        this.modifSommetButton.addActionListener(e -> {
            Sommet s = null;
            try {
                s = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            String[] optionType = switch (Objects.requireNonNull(s).getTypeSommet()) {
                case "M" -> new String[]{"Bloc opératoire", "Centre de nutrition"};
                case "N" -> new String[]{"Bloc opératoire", "Maternité"};
                case "O" -> new String[]{"Maternité", "Centre de nutrition"};
                default -> throw new IllegalStateException("Unexpected value: " + s.getTypeSommet());
            };
            int selectedOption = JOptionPane.showOptionDialog(null, "Choisissez le nouveau type de dispensaire","Modification type", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionType, optionType[0]);
            if (selectedOption == 0){
                s.setTypeSommetString(optionType[0]);
            } else if (selectedOption == 1){
                s.setTypeSommetString(optionType[1]);
            }
            dispose();
            fastMain(this.sommets, this.trajets);
        });
        this.modifTrajetButton.addActionListener(e -> {
            Trajet newTraj = null;
            try {
                newTraj = choixTrajet();
            } catch (ExceptionTrajetExistePas ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
            String[] optionTraj={"Fiabilité(%)", "Distance(km)", "Durée(min)"};
            int selectedOption = JOptionPane.showOptionDialog(null, "Choisissez le type de données à modifier","Type de modification", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionTraj, optionTraj[0]);
            String inputNewVal = JOptionPane.showInputDialog(null, "Tapez la nouvelle valeure de cette donnée");
            switch (selectedOption) {
                case 0 -> Objects.requireNonNull(newTraj).setFiab(Double.parseDouble(inputNewVal));
                case 1 -> Objects.requireNonNull(newTraj).setDist(Double.parseDouble(inputNewVal));
                case 2 -> Objects.requireNonNull(newTraj).setDur(Double.parseDouble(inputNewVal));
            }
            dispose();
            fastMain(this.sommets, this.trajets);
        });
        this.f10.addActionListener(e -> {
            Sommet s1 = null;
            try {
                s1 = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
            Sommet s2 = null;
            try {
                s2 = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            String type = choixType();
            StringBuilder retour = new StringBuilder();
            for (Integer i : sommets.listeVoisinsSpecifiqueDeuxSommets(Objects.requireNonNull(s1), s2, type)){
                JlabelDraggable temp = this.hashMapSommets.get(i);
                this.panelGraph.updateColor(temp, Color.RED);
                retour.append("S").append(i).append(", ");
            }
            if (retour.isEmpty()){
                retour.append("Il n'y a pas de sommet voisin à ces deux sommets de ce type !");
            }
            JOptionPane.showMessageDialog(null, retour);
            this.panelGraph.resetColor();
        });
        this.f11.addActionListener(e -> {
            Sommet s1 = null;
            try {
                s1 = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            Sommet s2 = null;
            try {
                s2 = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            if (sommets.deuxDistance(Objects.requireNonNull(s1), Objects.requireNonNull(s2))){
                JOptionPane.showMessageDialog(null, "Les sommets "+s1.getNomSommet()+" et "+s2.getNomSommet()+" sont à 2-distance");
            } else {
                JOptionPane.showMessageDialog(null, "Les sommets "+s1.getNomSommet()+" et "+s2.getNomSommet()+" ne sont pas à 2-distance ou sont voisins");
            }
        });
        this.f12.addActionListener(e -> {
            Sommet s1 = null;
            try {
                s1 = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            Sommet s2 = null;
            try {
                s2 = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            JOptionPane.showMessageDialog(null, this.sommets.compartVille(Objects.requireNonNull(s1))+this.sommets.compartVille(Objects.requireNonNull(s2)));
        });
        this.f13.addActionListener(e -> {
            Sommet s1 = null;
            try {
                s1 = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            Sommet s2 = null;
            try {
                s2 = choixSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            try {
                String retour = lcGraphe.cheminPlusCourt(Objects.requireNonNull(s1).getNomSommet(), Objects.requireNonNull(s2).getNomSommet());
                JOptionPane.showMessageDialog(null, retour+lcGraphe.complexitePlusCourtChemein());
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
        });
        this.f15.addActionListener(e -> {
            String s1 = null;
            try {
                s1 = choixSommet().getNomSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            String s2 = null;
            try {
                s2 = choixSommet().getNomSommet();
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
            try {
                JOptionPane.showMessageDialog(null, lcGraphe.cheminPlusFiable(s1, s2)+lcGraphe.cheminPlusRapide(s1, s2));
            } catch (ExceptionDispencereExistePas ex) {
                JOptionPane.showMessageDialog(null, "Erreur : "+ex);
            }
        });
        this.f19.addActionListener(e -> JOptionPane.showMessageDialog(null, lcGraphe.cheminTraversantChaqueType()));
        this.f20.addActionListener(e -> {
            JTextField nombreM = new JTextField();
            JTextField nombreN = new JTextField();
            JTextField nombreO = new JTextField();
            Object[] message = {
                    "Nombre de maternité:", nombreM,
                    "Nombre de centre de nutrition:", nombreN,
                    "Nombre de bloc opératoire:", nombreO
            };
            int option = JOptionPane.showConfirmDialog(null, message, "F20", JOptionPane.OK_CANCEL_OPTION);
            if (option == JOptionPane.OK_OPTION){
                int nbrM = Integer.parseInt(nombreM.getText());
                int nbrN = Integer.parseInt(nombreN.getText());
                int nbrO = Integer.parseInt(nombreO.getText());
                try {
                    JOptionPane.showMessageDialog(null, lcGraphe.cheminTraversantNSommetDeChaqueType(nbrM, nbrN, nbrO));
                } catch (ExceptionTypeNegatif | ExceptionTousNul ex) {
                    JOptionPane.showMessageDialog(null, "Erreur : "+ex);
                }
            }
        });
    }
    public static void fastMain(ListeSommets s, ListeTrajets t){
        GrapheIHM g = new GrapheIHM(s, t);
        g.setVisible(true);
        g.setLocation(0,0);
    }
}