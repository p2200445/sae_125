package IHM;

import CSV.ListeSommets;
import CSV.ListeTrajets;
import CSV.Trajet;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

public class PanelGraphe extends JPanel {
    private final ListeSommets sommets;
    private final List<Trajet> arretes;
    private final Map<Integer, JlabelDraggable> MapJlabel;
    private final Map<JlabelDraggable, Color> MapSomColor;
    private final Map<Trajet, String> MapTrajState;
    public PanelGraphe(ListeSommets newSommets, ListeTrajets newTrajets, Map<Integer, JlabelDraggable> newHashmapJLabel){
        this.sommets = newSommets;
        this.arretes = newTrajets.getListeTrajet();
        this.MapJlabel = newHashmapJLabel;
        this.MapSomColor = new HashMap<>();
        this.MapTrajState = new HashMap<>();
        setLayout(null);
        initComponents();
    }
    public void initComponents(){
        // Mise en place des positions des jlabels en deux cercles
        double angle = 2*Math.PI/sommets.getSize();
        int rayon = 175;
        int variation_r;
        int i = 0;
        for (JlabelDraggable jlabel : MapJlabel.values()){
            double theta = i * angle;
            if(i%2 == 0){
                variation_r = 100;
            }else{
                variation_r = 0;
            }
            double v1 = 320 + (rayon + variation_r) * Math.cos(theta);
            double v2 = 320 + (rayon + variation_r) * Math.sin(theta);
            int px = (int) v1;
            int py = (int) v2;
            add(jlabel);
            jlabel.setBounds(px, py, 50, 50);
            i++;
            this.MapSomColor.put(jlabel, Color.CYAN);
        }
        for (Trajet t : this.arretes){
            this.MapTrajState.put(t, "Num");
        }
    }
    public void updateState(String newState){
        for (Trajet t : arretes){
            this.MapTrajState.put(t, newState);
        }
    }
    public void resetState(){
        for (Trajet t : this.arretes){
            this.MapTrajState.put(t, "Num");
            repaint();
        }
    }
    public void updateColor(JlabelDraggable j, Color c){
        this.MapSomColor.put(j, c);
        repaint();
    }
    public void resetColor(){
        for (JlabelDraggable j : this.MapJlabel.values()){
            this.MapSomColor.put(j, Color.CYAN);
            repaint();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int numTrajet = 1;
        for (Trajet t : arretes){
            String value = switch (this.MapTrajState.get(t)) {
                case "Fiab" -> String.valueOf(t.getFiab());
                case "Dist" -> String.valueOf(t.getDist());
                case "Dur" -> String.valueOf(t.getDur());
                default -> String.valueOf(numTrajet);
            };
            Graphics2D g2d = (Graphics2D) g.create();
            JlabelDraggable circleStrt = MapJlabel.get(t.getOriNbr());
            JlabelDraggable circleEnd = MapJlabel.get(t.getDestNbr());
            Point strt = new Point(circleStrt.getX()+(45/2), circleStrt.getY()+(45/2));
            Point end = new Point(circleEnd.getX()+(45/2), circleEnd.getY()+(45/2));
            g2d.setColor(Color.GRAY);
            g2d.drawLine(strt.x,strt.y, end.x, end.y);
            g2d.drawString(value,(strt.x+end.x)/2,(strt.y+end.y)/2);
            numTrajet++;
        } // Dessine un rond autour de chaque JLabel
        for (JlabelDraggable j : MapJlabel.values()) {
            Graphics2D g2d = (Graphics2D) g.create();
            int width = j.getWidth();
            int height = j.getHeight();
            int diameter = Math.min(width, height) - 5; // Diamètre des ronds
            int x = j.getX();
            int y = j.getY();
            Color c = this.MapSomColor.get(j);
            g2d.setColor(c);
            g2d.fillOval(x, y, diameter, diameter);
            repaint();
        } // Dessine des traits pour chaque arrête
    }
}
