import CSV.ChargementGraphe;
import CSV.ListeSommets;
import CSV.ListeTrajets;
import IHM.GrapheIHM;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.File;

public class SAE_125 {
    public static File choixFichier(){
        File file = null;
        JFileChooser choose = new JFileChooser(FileSystemView.getFileSystemView());
        int res = choose.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            file = choose.getSelectedFile();
        }
        return file;
    } // Fonction qui demande un fichier à l'utilisateur
    public static void main(String[] args){
        ListeSommets s = new ListeSommets();
        ListeTrajets t = new ListeTrajets();
        File f1 = choixFichier();
        File f2 = choixFichier();
        ChargementGraphe.loadingGraph(f1, f2, s, t);
        GrapheIHM g = new GrapheIHM(s, t);
        g.setVisible(true);
        g.setLocation(0,0);

    }
}
