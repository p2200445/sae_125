package Exceptions;

public class ExceptionDispencereExistePas extends Exception {
    public ExceptionDispencereExistePas(String nomDisp) {
        super("Le dispencère " + nomDisp + " n'existe pas!");
    }
}
/*
  ExceptionDispencereExistePas de type Exception

  Patramètre :
  nomDisp de type String, le nom d'un dispencère.

  Sortie :
  de type String, message d'erreur disant que le nom
  du dispencère entrée en paramètre n'éxiste pas.

 */