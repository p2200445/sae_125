package Exceptions;

public class ExceptionTypeNegatif extends Exception {
    public ExceptionTypeNegatif(String typeSommet) {
        super("Le type de sommet " + typeSommet + " ne  peux pas être négatif!");
    }
}
/*
  ExceptionTypeNegatif de type Exception

  Paramètre :
  - typeSommet, de type String, le type d'un sommet.

  Sortie :
  - de type String, message d'erreur disant que le
  compteur du type de sommet passé en paramètre
  ne peut pas être négatif.
 */