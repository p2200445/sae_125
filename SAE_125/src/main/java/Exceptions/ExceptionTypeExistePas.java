package Exceptions;

public class ExceptionTypeExistePas extends Exception {
    public ExceptionTypeExistePas(String typeDisp) {
        super("Le type de dispencère " + typeDisp + " n'existe pas!");
    }
}
/*
  ExceptionTypeExistePas de type Exception

  Paramètre :
  - typeSommet, de type String, le type d'un sommet.

  Sortie :
  - de type String, message d'erreur disant que le
  type de sommet passé en paramètre n'éxiste pas.
 */