package Exceptions;

public class ExceptionTrajetExistePas extends Exception{
    public ExceptionTrajetExistePas(String nomTraj){
        super("Le trajet "+nomTraj+" n'existe pas!");
    }
}
