package Exceptions;

public class ExceptionTousNul extends Exception {
    public ExceptionTousNul() {
        super("Les compteurs de chaque type de sommet ne peuvent pas tous être à 0!");
    }
}
/*
  ExceptionTousNul de type Exception

  Paramètre :

  Sortie :
  - de type String, message d'erreur disant que les
  compteurs des types de sommet du graphe ne peuvent
  pas tous être à 0 à l'appel de la fonction.
 */