package GrapheMaillons;

/*
 * Cette classe permet de regrouper les compteur de chaque type de sommet dans un seul objet,
 * ce qui simplifie le renvois de certaines fonction dans LCGraphe.
 */
public class Triplet {
    private int m;
    private int n;
    private int o;

    /*
     * Constructeur de la classe
     */
    public Triplet(int m, int n, int o) {
        this.m = m;
        this.n = n;
        this.o = o;
    }

    /*
     * Renvois le compteur des matèrnités
     */
    public int getM() {
        return m;
    }

    /*
     * Initialise le compteur des matèrnités
     */
    public void setM(int m) {
        this.m = m;
    }

    /*
     * Renvois le compteur des centres de nutritions
     */
    public int getN() {
        return n;
    }

    /*
     * Initialise le compteur des centres de nutritions
     */
    public void setN(int n) {
        this.n = n;
    }

    /*
     * Renvois le Compteur des blocs opératoires
     */
    public int getO() {
        return o;
    }

    /*
     * Initialise le compteur de blocs opératoires
     */
    public void setO(int o) {
        this.o = o;
    }

    /*
     * Affiche les données de l'objet de la classe
     */
    @Override
    public String toString() {
        return "Triplet [m=" + m + ", n=" + n + ", o=" + o + "]";
    }

}
