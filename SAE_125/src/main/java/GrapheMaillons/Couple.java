package GrapheMaillons;
/*
 * Cette classe permet de simplifier l'utilisation d'un objet de la classe Map
 * dans les algorithme Dijkstra dans la classe LCGraphe.
 */
public class Couple {
    public String nom;
    public double val;
    /*
     * Constucteur de la classe
     */
    public Couple(String name, double i) {
        this.nom = name;
        this.val = i;
    }

    /*
     * Renvois le nom du sommet par lequel on à la valeur la plus intéressante dans
     * l'algorithme Disjkstra
     */
    public String getNom() {
        return this.nom;
    }

    /*
     * Renvois la valeur la plus intéressante dans l'algorithme Disjkstra
     */
    public double getVal() {
        return val;
    }

    /*
     * Affiche les données de l'objet de la classe
     */
    @Override
    public String toString() {
        return "Couple [nom=" + nom + ", val=" + val + "]";
    }
}
