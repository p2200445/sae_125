package GrapheMaillons;

import Exceptions.ExceptionDispencereExistePas;
import Exceptions.ExceptionTousNul;
import Exceptions.ExceptionTypeExistePas;
import Exceptions.ExceptionTypeNegatif;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class LCGraphe {
    public static class MaillonGrapheSec {
        private final double fiab;
        private final double dist;
        private final double dur;
        private final String dest;
        private MaillonGrapheSec suiv;

        private MaillonGrapheSec(double f, double dt, double dr, String d) {
            fiab = f;
            dist = dt;
            dur = dr;
            dest = d;
            suiv = null;
        }
    }

    static class MaillonGraphe {
        private final String nom;
        private final String type;
        private MaillonGrapheSec lVois;
        private MaillonGraphe suiv;
        private boolean listed;

        MaillonGraphe(String n, String t) {
            nom = n;
            type = t;
            lVois = null;
            suiv = null;
            listed = false;

        }
    }

    private MaillonGraphe premier;

    public LCGraphe() {
        premier = null;
    }

    public void addMain(String ori, String t) {
        MaillonGraphe nouv = new MaillonGraphe(ori, t);
        nouv.suiv = this.premier;
        this.premier = nouv;
    }

    public void addarrete(String o, String d, double fiab, double dist, double dur) {
        MaillonGrapheSec nouv = new MaillonGrapheSec(fiab, dist, dur, d);
        MaillonGraphe tmp = this.premier;
        while (!tmp.nom.equals(o)) {
            tmp = tmp.suiv;
        }
        nouv.suiv = tmp.lVois;
        tmp.lVois = nouv;

        MaillonGrapheSec nouv2 = new MaillonGrapheSec(fiab, dist, dur, o);
        MaillonGraphe tmp2 = this.premier;
        while (!tmp2.nom.equals(d)) {
            tmp2 = tmp2.suiv;
        }
        nouv2.suiv = tmp2.lVois;
        tmp2.lVois = nouv2;
    }

    public String toString() {
        StringBuilder retour = new StringBuilder();
        MaillonGraphe tmp = this.premier;
        while (tmp != null) {
            retour.append(tmp.nom).append(" : ");
            MaillonGrapheSec tmp2 = tmp.lVois;
            while (tmp2 != null) {
                retour.append(tmp2.dest).append("(fiab : ").append(tmp2.fiab).append(",duree : ").append(tmp2.dur)
                        .append(", dist :  ").append(tmp2.dist).append("),");
                tmp2 = tmp2.suiv;

            }
            retour.append("\n");
            tmp = tmp.suiv;
        }

        return retour.toString();
    }

    /**
     * listevoisons(String dispencere)
     * <p>
     * dispencere String - le nom d'un dispencere.
     * 
     * @return La liste de tout les voisins du sommet en parametre
     *         <p>
     *         Cette fonction met dans une liste de String tout les sommets qui
     *         font partie de la sous-chaine du maillon de la chaine principale
     *         qui correspond au sommet en parametre.
     */
    public List<String> listevoisons2(String dispencere) {
        List<String> rep = new ArrayList<>();
        MaillonGraphe tmp = this.premier;
        while (tmp != null && !tmp.nom.equals(dispencere)) {
            tmp = tmp.suiv;
        }
        if (tmp != null) {
            MaillonGrapheSec tmp2 = tmp.lVois;
            while (tmp2 != null) {
                if (estVisite(tmp2.dest)) {
                    rep.add(tmp2.dest);
                }
                tmp2 = tmp2.suiv;
            }
        }
        return rep;
    }

    /* F9 */
    /**
     * listevoisinsspecifique(dispencere, type)
     * <p>
     * dispencere String - Nom d'un dispencere
     * type String - type d'un dispencere
     * 
     * @return liste de tous les voisins du sommet en parrametre et
     *         qui sont du type en parametre
     * @throws ExceptionDispencereExistePas Exception qui permet de s'assurer que
     *                                      le dispencere est bien existant.
     * @throws ExceptionTypeExistePas       Exception qui permet de s'assurer que
     *                                      le type de sommet t est bien existant.
     *                                      <p>
     *                                      Cette fonction met dans un String tout
     *                                      les sommets qui font partie de la
     *                                      sous-chaine du maillon de la chaine
     *                                      principale qui correspond au sommet en
     *                                      parametre et qui sont du type passé en
     *                                      parametre.
     */
    public String listevoisonsspecifique(String dispencere, String type)
            throws ExceptionDispencereExistePas, ExceptionTypeExistePas {
        if (!contient(dispencere)) {
            throw new ExceptionDispencereExistePas(dispencere);
        }
        if (!type.equals("M") && !type.equals("N") && !type.equals("O")) {
            throw new ExceptionTypeExistePas(type);
        }
        StringBuilder rep = new StringBuilder();
        MaillonGraphe tmp = this.premier;
        while (!tmp.nom.equals(dispencere)) {
            tmp = tmp.suiv;
        }

        MaillonGrapheSec tmp2 = tmp.lVois;
        while (tmp2 != null) {
            MaillonGraphe tmp3 = this.premier;
            while (!tmp3.nom.equals(tmp2.dest)) {
                tmp3 = tmp3.suiv;
            }
            if (tmp3.type.equals(type)) {
                rep.append(tmp2.dest).append(", ");
            }
            tmp2 = tmp2.suiv;

        }

        return rep.toString();
    }

    /* F10 */
    /**
     * listevoisonsspecifique2sommets(dispencere1, dispencere2, type)
     * <p>
     * dispencere1 String - nom d'un dispencere
     * dispencere2 String - nom d'un dispencere
     * type String - type de dispencere
     * 
     * @return liste des voisins de dispencere1 et de dispencere2 qui sont du type
     *         type
     *         <p>
     *         Cette fonction appel deux fois listevoisonsspecifique, pour chacun
     *         des deux dispenceres en entrée et stock les retours dans un String
     *         avant de le renvoyer
     */
    public String listevoisinsspecifique2sommets(String dispencere1, String dispencere2, String type) {
        String retour = "";
        try {
            retour += this.listevoisonsspecifique(dispencere1, type);
        } catch (ExceptionDispencereExistePas | ExceptionTypeExistePas pbm) {
            System.out.print("Error : " + pbm);
        }
        try {
            retour += this.listevoisonsspecifique(dispencere2, type);
        } catch (ExceptionDispencereExistePas | ExceptionTypeExistePas pbm) {
            System.out.print("Error : " + pbm);
        }

        return retour;
    }

    /* F11 */
    /**
     * deuxdistance(dispencer1, dispencere2)
     * <p>
     * dispencere1 String - Nom d'un dispencere
     * dispencere2 String - Nom d'un dispencere
     * 
     * @return boolean, true si les deux dispenceres sont exactement à 2 de
     *         distance, false sinon
     * 
     * @throws ExceptionDispencereExistePas Exception qui permet de s'assurer que
     *                                      le dispencere est bien existant.
     *                                      <p>
     *                                      Cette fonction cherche s'il y a au moins
     *                                      un voisins commun au deux dispencere et
     *                                      s'assure que les deux dispencere ne
     *                                      soient
     *                                      pas voisins.
     */
    public boolean deuxdisantce(String dispencere1, String dispencere2) throws ExceptionDispencereExistePas {
        if (!contient(dispencere1)) {
            throw new ExceptionDispencereExistePas(dispencere1);
        }
        if (!contient(dispencere2)) {
            throw new ExceptionDispencereExistePas(dispencere2);
        }
        boolean b = false;
        MaillonGraphe tmp = this.premier;
        MaillonGraphe tmp2 = this.premier;
        while (!tmp.nom.equals(dispencere1)) {
            tmp = tmp.suiv;
        }
        while (!tmp2.nom.equals(dispencere2)) {
            tmp2 = tmp2.suiv;
        }
        MaillonGrapheSec tmp3 = tmp.lVois;
        while (tmp3 != null && !b) {
            MaillonGrapheSec tmp4 = tmp2.lVois;
            while (tmp4 != null && !b) {
                if (tmp3.dest.equals(tmp4.dest)) {
                    b = true;
                }
                tmp4 = tmp4.suiv;
            }
            tmp3 = tmp3.suiv;

        }
        return b;

    }

    /* F12 */
    /**
     * compartdeuxville(dispencer1, dispencer2)
     * <p>
     * dispencere1 String - nom d'un dispencere
     * dispencere2 String - nom d'un dispencere
     * 
     * @return Liste des diférence entre les voisins des deux dispenceres
     * 
     * @throws ExceptionDispencereExistePas Exception qui permet de s'assurer que
     *                                      le dispencere est bien existant.
     *                                      <p>
     *                                      Cette fonction va lister toutes les
     *                                      carctéristiques des sommets qui sont à
     *                                      plus de deux distance de disp1 et de
     *                                      disp2.
     */
    public String compartdeuxville(String dispencere1, String dispencere2) throws ExceptionDispencereExistePas {
        if (!contient(dispencere1)) {
            throw new ExceptionDispencereExistePas(dispencere1);
        }
        if (!contient(dispencere2)) {
            throw new ExceptionDispencereExistePas(dispencere2);
        }
        String retour = "";

        int m1 = 0, o1 = 0, n1 = 0, m2 = 0, o2 = 0, n2 = 0;
        MaillonGraphe tmp = this.premier;

        while (tmp != null) {
            if (!deuxdisantce(tmp.nom, dispencere1) && !tmp.nom.equals(dispencere1)
                    && !estVoisin(dispencere1, tmp.nom)) {
                switch (tmp.type) {
                    case "m" -> m1++;
                    case "o" -> o1++;
                    case "n" -> n1++;
                }
            } else if (!deuxdisantce(tmp.nom, dispencere2) && !tmp.nom.equals(dispencere2)
                    && !estVoisin(dispencere2, tmp.nom)) {
                switch (tmp.type) {
                    case "m" -> m2++;
                    case "o" -> o2++;
                    case "n" -> n2++;
                }

            }
            tmp = tmp.suiv;
        }
        retour += "Il y a " + m1 + " maternités à plus de 2 de distance de la ville de " + dispencere1 + ". \n" +
                "Il y a " + o1 + " bloques opératoires à plus de 2 de distance de la ville de " + dispencere1 + ". \n" +
                "Il y a " + n1 + " centres de nutritions à plus de 2 de distance de la ville de " + dispencere1 + ". \n"
                +
                "Il y a " + m2 + " maternités à plus de 2 de distance de la ville de " + dispencere2 + ". \n" +
                "Il y a " + o2 + " bloques opératoires à plus de 2 de distance de la ville de " + dispencere2 + ". \n" +
                "Il y a " + n2 + " centres de nutritions à plus de 2 de distance de la ville de " + dispencere2
                + ". \n";

        return retour;
    }

    /**
     * estVoisin(dispencer1, dispencere2)
     * <p>
     * dispencere1 String - nom d'un dispencere
     * dispencere2 String - nom d'un dispencere
     * 
     * @return true si les deux dispenceres sont voisins dans le graphe, false sinon
     *         <p>
     *         Cette fonction cherche dabord disp1 dans le liste principale de la
     *         structure de données, puis cherche disp2 dans la liste des voisins de
     *         disp1.
     *         Si disp2 est trouver dans les voisins de disp1 alors la fonction
     *         renvois true, false sinon.
     */
    public boolean estVoisin(String dispencere1, String dispencere2) {
        boolean b = false;
        MaillonGraphe tmp = this.premier;
        while (!tmp.nom.equals(dispencere1)) {
            tmp = tmp.suiv;
        }
        MaillonGrapheSec tmp2 = tmp.lVois;
        while (tmp2 != null && !b) {
            if (tmp2.dest.equals(dispencere2)) {
                b = true;
            }
            tmp2 = tmp2.suiv;
        }
        return b;
    }

    /* F13 */
    /**
     * getCoup(disp1, disp2, i)
     * <p>
     * disp1 String - nom d'un dispencere
     * disp2 String - nom d'un dispencere
     * i int - pour différencier le type de parcourt
     * 
     * @return le coup pour aller de dispencere1 à dispencere2
     *         <p>
     *         Cette fonction cherche dabord disp1 dans le liste principale de la
     *         structure de données, puis cherche disp2 dans la liste des voisins de
     *         disp1 puis renvois soit la dispence si i = 1, soit la durée si i = 2,
     *         soit la fiabilité si i = 3 du chemin entre disp1 et disp2.
     */
    public double getCoup(String disp1, String disp2, int i) {
        double d = 0;
        MaillonGraphe tmp = this.premier;
        while (!tmp.nom.equals(disp1)) {
            tmp = tmp.suiv;
        }
        MaillonGrapheSec tmp2 = tmp.lVois;
        while (tmp2 != null && !tmp2.dest.equals(disp2)) {
            tmp2 = tmp2.suiv;
        }
        if (tmp2 != null) {
            switch (i) {
                case 1 -> d = tmp2.dist;
                case 2 -> d = tmp2.dur;
                case 3 -> d = tmp2.fiab;
            }
        }

        return d;
    }

    /**
     * estVisite(dispencere)
     * <p>
     * dispencere String - nom d'un dispencere
     * 
     * @return true le dispencer est déjà visité
     *         <p>
     *         Cette fonction parcour la chaine principale de la srtucture de
     *         données et quand elle trouve le sommet dont le nom est en paramettre,
     *         elle check si ce sommet est visited (si le boolean listed est à
     *         true).
     */
    public boolean estVisite(String dispencere) {
        MaillonGraphe tmp = this.premier;
        while (!tmp.nom.equals(dispencere)) {
            tmp = tmp.suiv;
        }
        return !tmp.listed;
    }

    /**
     * setVisite(disp)
     * <p>
     * disp String - Nom d'un diospencere
     * <p>
     * Passe le sommet de non visité à visité
     */
    public void setVisite(String disp) {
        MaillonGraphe tmp = this.premier;
        while (tmp != null) {
            if (tmp.nom.equals(disp)) {
                tmp.listed = true;
            }
            tmp = tmp.suiv;
        }
    }

    /**
     * annulEstVisiter()
     * <p>
     * Passe tout les sommets visité en non visité
     */
    public void annulEstVisiter() {
        MaillonGraphe tmp = this.premier;
        while (tmp != null) {
            tmp.listed = false;
            tmp = tmp.suiv;
        }

    }

    /**
     * plusgrand(mapp)
     * <p>
     * mapp map / dictionnaire avec pour clef des noms de dispencere de typre
     * String et pour valeur des Couples(le nom d'un autre dispencere de
     * type String + la valeur du coup pour parvenir dispencere de clef,
     * de type double)
     * 
     * @return le nom du dispencere pour lequel la valeur pour y parvenir est la
     *         plus forte
     * 
     *         Cette algorithme parcour tout mapp et cherche la valeur la plus forte
     */
    public String plusgrand(Map<String, Couple> mapp) {
        double maxi = 0;
        String clefMini = null;
        for (String clef : mapp.keySet()) {
            if (mapp.get(clef).getVal() > maxi && estVisite(clef)) {
                maxi = mapp.get(clef).getVal();
                clefMini = clef;
            }
        }
        return clefMini;
    }

    /**
     * plusfiable(disp1, disp2)
     * <p>
     * disp1 String - nom d'un dispencere
     * disp2 String - nom d'un dispencere
     * 
     * @return le chemin le plus fiable pour aller de disp1 à disp2
     *         <p>
     * 
     *         Cette fonction utilise l'algorithme
     *         d'Dijkstra pour trouver le chemin le
     *         plus fiable pour aller de dispencere1
     *         à dispencer2.
     */
    public String plusfiable(String disp1, String disp2) {
        /* Initilisation des variables */
        StringBuilder retour = new StringBuilder();
        double fiabtmp;
        List<String> aParcourir = new ArrayList<>(); /* List de tout les sommets qui doivent être parcouru */
        List<String> voisinAParcourir;
        Map<String, Couple> tab = new HashMap<>();
        MaillonGraphe tmp = this.premier;
        String maxi;
        String remonte = disp2;

        /* Initialisation du dico */
        while (tmp != null) {
            tab.put(tmp.nom, new Couple(null, 0));
            aParcourir.add(tmp.nom);
            tmp = tmp.suiv;
        }
        tab.replace(disp1, new Couple(null, 1));

        /* Algo Dijkstra */
        while (aParcourir.size() != 0) {/* pour tout les sommet du graphe */
            maxi = plusgrand(tab);
            voisinAParcourir = listevoisons2(maxi);
            for (String sommet : voisinAParcourir) { /* pour tout les voisins du sommet étudié */
                if (estVisite(sommet)) {/* si le sommet n'est pas visité */
                    fiabtmp = tab.get(maxi).getVal() * (getCoup(maxi, sommet, 3) / 10);/* calcul du nouveau coup */
                    if (fiabtmp > tab.get(sommet).getVal()) {/* remplacement du coup si le nouveau est mieux */
                        tab.replace(sommet, new Couple(maxi, fiabtmp));
                    }
                }
            }

            aParcourir.remove(maxi);
            setVisite(maxi);
        }

        while (remonte != null) {/* reconstitution du chemin optimal */
            retour.insert(0, remonte + ", ");
            remonte = tab.get(remonte).getNom();
        }

        retour = new StringBuilder("Le chemin le plus fiable entre " + disp1 + " et " + disp2 + " est : \n" + retour
                + " avec une fiabilité de " + tab.get(disp2).getVal());

        /* Oncheck est visité */
        this.annulEstVisiter();
        return retour.toString();
    }

    /**
     * cheminPlusFiable(d1, d2)
     * <p>
     * d1 String - nom d'un dispencere
     * d2 String - nom d'un dispencere
     * 
     * @return le chemin le plus fiable pour aller de disp1 à disp2 (avec plus de
     *         mise en page)
     * 
     * @throws ExceptionDispencereExistePas Exception qui permet de s'assurer que
     *                                      le dispencere est bien existant.
     *                                      <p>
     *                                      Cette fonction fait un appel à
     *                                      cheminPlusFiable() avec disp1 et disp2
     *                                      en parametre
     */
    public String cheminPlusFiable(String d1, String d2) throws ExceptionDispencereExistePas {
        if (!contient(d1)) {
            throw new ExceptionDispencereExistePas(d1);
        }
        if (!contient(d2)) {
            throw new ExceptionDispencereExistePas(d2);
        }

        return plusfiable(d1, d2);
    }

    /* F14 */
    /**
     * complexitePlusCourtChemein()
     * 
     * @return Le résultat calcul de la compléxité de la fonction cheminPlusFianble
     */
    public String complexitePlusCourtChemein() {
        return "La compléxité de l'algorithme de calcul du plus court chemin est d'environ 12n²+6n+26 opérations dans le pire des cas";
    }

    /* F15 */
    /**
     * pluscourtch(dispencere1, dispencere2, i)
     * <p>
     * dispencere1 String - nom d'un dispencere
     * dispencere2 String - nom d'un dispencere
     * i int - pour différencier le type de parcourt
     * 
     * @return la chemin le plus court ou le plus rapide pour aller de dispencere1 à
     *         dispencere2
     *         <p>
     *         Cette fonction utilise l'algorithme d'Dijkstra pour trouver le chemin
     *         le plus court ou le plus rapide pour aller de dispencere1 à
     *         dispencer2.
     */
    public String pluscourtch(String dispencere1, String dispencere2, int i) {
        /* Initilisation des variables */
        StringBuilder retour = new StringBuilder();
        double coup;
        List<String> aParcourir = new ArrayList<>();
        List<String> voisinAParcourir;
        Map<String, Couple> tab = new HashMap<>();
        MaillonGraphe tmp = this.premier;
        String pp;
        String remonte = dispencere2;

        /* Initialisation du dico */
        while (tmp != null) {
            tab.put(tmp.nom, new Couple(null, 1000));
            aParcourir.add(tmp.nom);
            tmp = tmp.suiv;
        }
        tab.replace(dispencere1, new Couple(null, 0));

        /* Algo Dijkstra */
        while (aParcourir.size() != 0) { /* pour tout les sommets du graphe */
            pp = pluspetit(tab);
            voisinAParcourir = listevoisons2(pp);
            for (String sommet : voisinAParcourir) { /* pour tout les voisins du sommet étudié */
                if (estVisite(sommet)) {
                    coup = tab.get(pp).getVal() + getCoup(pp, sommet, i);/* calcul du nouveau coup */
                    if (coup < tab.get(sommet).getVal()) {/* remplacement du trajet si le nouveau est mieux */
                        tab.replace(sommet, new Couple(pp, coup));
                    }

                }
            }
            aParcourir.remove(pp);
            setVisite(pp);
        }
        /* Reconstitucion du chemin */
        while (remonte != null) {
            retour.insert(0, remonte + ", ");
            remonte = tab.get(remonte).getNom();
        }

        switch (i) {
            case 1 ->
                retour = new StringBuilder(
                        "Le chemin le plus court entre " + dispencere1 + " et " + dispencere2 + " est : \n" + retour
                                + "avec une distance de " + tab.get(dispencere2).getVal() + " km.\n");
            case 2 ->
                retour = new StringBuilder(
                        "Le chemin le plus rapide entre " + dispencere1 + " et " + dispencere2 + " est : \n" + retour
                                + "avec un temps de " + tab.get(dispencere2).getVal() + " min.\n");
        }

        /* Oncheck est visité */
        this.annulEstVisiter();

        return retour.toString();
    }

    /**
     * pluspetit(mapp)
     * <p>
     * mapp Map<String, Couple> - dictionnaire des coup
     * 
     * @return la clef pour laquelle le coup est minimal
     *         <p>
     *         Cette fonction parcour l'ensemble de mapp est cherche la ckef pour
     *         laquelle la valeur (contenu dans le Couple) est minimal, cette clef
     *         est ensuite renvoyer.
     */
    public String pluspetit(Map<String, Couple> mapp) {
        double mini = 1000;
        String clefMini = null;
        for (String clef : mapp.keySet()) {
            if (mapp.get(clef).getVal() < mini && estVisite(clef)) {
                mini = mapp.get(clef).getVal();
                clefMini = clef;
            }
        }
        return clefMini;
    }

    /**
     * cheminPlusRapide(d1, d2)
     * <p>
     * d1 String - nom d'un dispencere
     * d2 String - nom d'un dispencere
     * 
     * @return le chemin le plus rapide pour aller de d1 à d2
     * 
     * @throws ExceptionDispencereExistePas Exception qui permet de s'assurer que
     *                                      le dispencere est bien existant.
     *                                      <p>
     *                                      Cette fonction fait un appel à
     *                                      pluscortch() avec d1 et d2 en parametre
     *                                      ainsi que le int qui faut en parmetre
     *                                      pour que la fonction appeler calcul bien
     *                                      le chemin le plus rapide.
     */
    public String cheminPlusRapide(String d1, String d2) throws ExceptionDispencereExistePas {
        if (!contient(d1)) {
            throw new ExceptionDispencereExistePas(d1);
        }
        if (!contient(d2)) {
            throw new ExceptionDispencereExistePas(d2);
        }
        return pluscourtch(d1, d2, 2);
    }

    /**
     * contient(nomDisp)
     * <p>
     * nomDisp String - nom d'un dispencere
     * 
     * @return true si le dispencere est dans la structure de donnée
     *         <p>
     *         Cette fonction parcour la chaine principale de la structure de
     *         données et si le nom du dispencere en parametre est trouver un
     *         boolean est passé à true(le boolean est initialisier à false par
     *         défaut au début de la fonction), à la fin de la fonction le boolean
     *         est renvoyer
     */
    public boolean contient(String nomDisp) {
        MaillonGraphe tmp = this.premier;
        boolean b = false;
        while (tmp != null) {
            if (tmp.nom.equals(nomDisp)) {
                b = true;
                break;
            }
            tmp = tmp.suiv;
        }
        return b;
    }

    /**
     * cheminPlusCourt(d1, d2)
     * <p>
     * d1 String - nom d'un dispencere
     * d2 String - nom d'un dispencere
     * 
     * @return chemil le plus court pour aller de d1 à d2
     * 
     * @throws ExceptionDispencereExistePas Exception qui permet de s'assurer que
     *                                      le dispencere est bien existant.
     *                                      <p>
     *                                      Cette fonction fait un appel à
     *                                      pluscortch() avec d1 et d2 en parametre
     *                                      ainsi que le int qui faut en parmetre
     *                                      pour que la fonction appeler calcul bien
     *                                      le chemin le plus court
     */
    public String cheminPlusCourt(String d1, String d2) throws ExceptionDispencereExistePas {
        if (!contient(d1)) {
            throw new ExceptionDispencereExistePas(d1);
        }
        if (!contient(d2)) {
            throw new ExceptionDispencereExistePas(d2);
        }
        return pluscourtch(d1, d2, 1);
    }

    /* F19 */
    /**
     * typecompa(disp, m, o, n)
     * <p>
     * disp String - nom d'un dispencere
     * m int - nombre de maternité à parcourir
     * n int - nombre de centre de nutrition à parcourir
     * o int - nombre de bloc opératoire à parcourir
     * 
     * @return true si le compteur correspondant au type du dispencere est > 0
     *         <p>
     *         Cette fonction recupere le type du dispencere dont le nom est en
     *         parmetre et compart si le compteur correspondant à ce type est > 0
     */
    public boolean typecompa(String disp, Triplet compteurs) {
        boolean b = false;
        MaillonGraphe tmp = this.premier;

        while (tmp != null && !tmp.nom.equals(disp)) {
            tmp = tmp.suiv;
        }
        if (tmp != null && !tmp.listed) {
            switch (tmp.type) {
                case "M" -> b = compteurs.getM() > 0;
                case "O" -> b = compteurs.getO() > 0;
                case "N" -> b = compteurs.getN() > 0;
            }
        }

        return b;
    }

    /**
     * modifCompteur(typesommet, m, n, o, revers)
     * <p>
     * typesommet Stirng - type d'un sommet
     * m int - nombre de maternité à parcourir
     * n int - nombre de centre de nutrition à parcourir
     * o int - nombre de bloc opératoire à parcourir
     * revers boolean - true pour décrémenter, false pour incrémenter
     * 
     * @throws ExceptionTypeExistePas Exception qui permet de s'assurer que
     *                                le type de dispencere est bien existant.
     *                                <p>
     *                                Cette fonction incrémente ou décrémente le
     *                                conteur du type passé en parametre, suivant le
     *                                boolean en parametre
     */
    public Triplet modifCompteur(String typesommet, Triplet compteurs, boolean revers)
            throws ExceptionTypeExistePas {
        if (!typesommet.equals("M") && !typesommet.equals("N") && !typesommet.equals("O")) {
            throw new ExceptionTypeExistePas(typesommet);
        }
        switch (typesommet) {
            case "M" -> {
                if (revers) {
                    compteurs.setM(compteurs.getM() - 1);
                } else {
                    compteurs.setM(compteurs.getM() + 1);
                }
            }
            case "N" -> {
                if (revers) {
                    compteurs.setN(compteurs.getN() - 1);
                } else {
                    compteurs.setN(compteurs.getN() + 1);
                }
            }
            case "O" -> {
                if (revers) {
                    compteurs.setO(compteurs.getO() - 1);
                } else {
                    compteurs.setO(compteurs.getO() + 1);
                }
            }
        }
        return compteurs;
    }

    /**
     * getType(name)
     * <p>
     * name String - nom d'un dispencere
     * 
     * @return le type du dispencere
     * 
     * @throws ExceptionDispencereExistePas Exception qui permet de s'assurer que
     *                                      le dispencere est bien existant.
     *                                      <p>
     *                                      Cette fonction va cherhcer le type du
     *                                      nom du dispencere parametre, dans la
     *                                      structure de données
     */
    public String getType(String name) throws ExceptionDispencereExistePas {
        MaillonGraphe tmp = this.premier;
        while (tmp != null && !tmp.nom.equals(name)) {
            tmp = tmp.suiv;
        }
        if (tmp == null) {
            throw new ExceptionDispencereExistePas(name);
        }
        return tmp.type;
    }

    /**
     * chemin(m, n, o)
     * <p>
     * m int - nombre de maternité à parcourir
     * n int - nombre de centre de nutrition à parcourir
     * o int - nombre de bloc opératoire à parcourir
     * 
     * @return un chemin passant par le nombre de chaque type de sommet
     *         <p>
     *         Cette fonction appel rec_chemin() pour tout les msommets du graphe
     *         qui sont tour à tour placer dans une List de String, cette fonction
     *         crée aussi une List de List de String pour pouvoir stocké les chemins
     *         qui passe par le nombre de chaque type de sommet en parametre.
     */
    public List<String> chemin(Triplet compteurs) {
        List<List<String>> tab = new ArrayList<>();
        List<String> ch = new ArrayList<>();
        ObjetRec tabch;
        MaillonGraphe tmp = this.premier;
        while (tmp != null) { /* pour tous les sommets du graphe */
            tabch = new ObjetRec(tab, ch, compteurs, tmp.nom);
            tabch.addsommet(tmp.nom);
            try {/* décrémentation du compteur du type du sommet étudié */
                compteurs = modifCompteur(tmp.type, compteurs, true);
            } catch (ExceptionTypeExistePas pbm) {
                System.out.print("Error : " + pbm);
            }
            tabch = rec_chemin(tabch);/* recherche de tout les chemin possible à partir du sommet étudié */
            try {/* incrémentation du compteur du type du sommet étudié */
                compteurs = modifCompteur(tmp.type, compteurs, false);
            } catch (ExceptionTypeExistePas pbm) {
                System.out.print("Error : " + pbm);
            }
            ch.remove(tmp.nom);
            tmp = tmp.suiv;
        }

        Random random = new Random();/* choix d'un chemin aléatoir parmis tous les chemins trouvés */
        int i = random.nextInt(tab.size());

        return tab.get(i);
    }

    /**
     * getMaillon(nomDisp)
     * <p>
     * nomDisp String - nom du dispencere
     * 
     * @return le maillon rechercher
     * @throws ExceptionDispencereExistePas Exception qui permet de s'assurer que
     *                                      le dispencere est bien existant.
     *                                      <p>
     *                                      Cette fonction parcour la liste
     *                                      principal de la structure de donné et
     *                                      renvois le maillon rechercher
     */
    public MaillonGraphe getMaillon(String nomDisp) throws ExceptionDispencereExistePas {
        MaillonGraphe tmp = this.premier;
        while (tmp != null && !tmp.nom.equals(nomDisp)) {
            tmp = tmp.suiv;
        }
        if (tmp == null) {
            throw new ExceptionDispencereExistePas(nomDisp);
        }
        return tmp;
    }

    /**
     * rec_chemin(tab, ch, m, n, o)
     * <p>
     * tab List<List<String>> - équivalant d'un tableau de tableu permettant
     * de stocké tous les chemins
     * ch List<String> - Chemin en cours d'étude
     * m int - nombre de sommet du type maternié qu'il reste à parcourir
     * n int - nombre de sommet du type cnetre de nutrition qu'il reste à
     * parcourir
     * o int - nombre de sommet du type bloc opératoire qu'il reste à
     * parcourir
     * <p>
     * Modifie récursivement et intrasequement tab en le remplicant de la
     * liste des chimen qui passe par le bon nombre de chaque type de
     * sommet
     */
    public ObjetRec rec_chemin(ObjetRec recurtion) {
        try {
            /* Init varible interne de la fct */
            MaillonGraphe tmp = getMaillon(recurtion.getChemin().get(recurtion.getChemin().size() - 1));
            /* récupération du dernier sommet entrer dans le chemin en construction */
            List<String> listeVoison = listevoisons2(tmp.nom);
            /* récupération des voisins du sommet récupérer */

            if (!tmp.nom.equals(recurtion.getRacine()) || !recurtion.isRacinedejavu()) {
                /* si le chemin ne passe pas par tous les sommets demander */
                recurtion.setRacinedejavu(true);
                /* Cas si on doit/peut encore parcourir le graphe */

                if (recurtion.getCompteurs().getM() > 0 || recurtion.getCompteurs().getN() > 0
                        || recurtion.getCompteurs().getO() > 0) {

                    for (String voisin : listeVoison) { /* étude de tous les voisins du sommet récupérer */

                        if (typecompa(voisin, recurtion.getCompteurs())) {
                            recurtion.addsommet(voisin);

                            try {/* décrémentation du compteur du type du sommet étudié */
                                recurtion.setCompteurs(modifCompteur(getType(voisin), recurtion.getCompteurs(), true));
                            } catch (ExceptionTypeExistePas pbm) {
                                System.out.print("Error : " + pbm);
                            }

                            recurtion = rec_chemin(recurtion);/*
                                                               * choix d'un chemin aléatoir parmis tous les chemins
                                                               * trouvés
                                                               */

                            try {/* incrémentation du compteur du type du sommet étudié */
                                recurtion.setCompteurs(modifCompteur(getType(voisin), recurtion.getCompteurs(), false));
                            } catch (ExceptionTypeExistePas pbm) {
                                System.out.print("Error : " + pbm);
                            }

                            recurtion.rmvsommet(voisin);
                        }
                    }
                    /* Cas lorsqu'on à trouver un chemin */
                } else if (recurtion.getCompteurs().getM() == 0 && recurtion.getCompteurs().getN() == 0
                        && recurtion.getCompteurs().getO() == 0) {

                    recurtion.addchemin(recurtion.getChemin());
                    /* sauvegarde du chemin */
                }
            }
        } catch (ExceptionDispencereExistePas pbm) {
            System.out.print("Error : " + pbm);
        }
        return recurtion;

    }

    /**
     * cheminTraversantChaqueType()
     * 
     * @return un chemin passant par un sommet de chaque type.
     *         <p>
     *         Cette fonction appel chemin(m, n, o) et met en
     *         forme le résultat de cet appel
     */
    public String cheminTraversantChaqueType() {
        Triplet compteurs = new Triplet(1, 1, 1);
        List<String> tmp = chemin(compteurs);
        StringBuilder res = new StringBuilder("Un chemin passant par chaque type de sommet est : ");
        for (String sommet : tmp) {
            res.append(sommet).append(", ");
        }
        return res.toString();
    }

    /* F20 */
    /**
     * cheminTraversantNSommetDeChaqueType(m, n, o)
     * <p>
     * m int - nombre de sommet du type maternitée
     * n int - nombre de sommet du type centre de nutrition
     * o int - nombre de sommet du type bloc opératoire
     * 
     * @return un chemin passant par le nombre de chauque type de sommet données en
     *         entrée
     * 
     * @throws ExceptionTousNul     Exception qui assure que tous les types de
     *                              sommet ne soient pas tous nul en entré
     * @throws ExceptionTypeNegatif Exception qui assure que le nombre d'un type de
     *                              sommet ne soit pas négatif
     *                              <p>
     *                              Cette fonction appel chemin(m, n, o) et met en
     *                              forme le résultat de cet appel
     */
    public String cheminTraversantNSommetDeChaqueType(int m, int n, int o)
            throws ExceptionTousNul, ExceptionTypeNegatif {
        if (m == 0 && n == 0 && o == 0) {
            throw new ExceptionTousNul();
        } else if (m < 0) {
            throw new ExceptionTypeNegatif("M");
        } else if (n < 0) {
            throw new ExceptionTypeNegatif("N");
        } else if (o < 0) {
            throw new ExceptionTypeNegatif("O");
        }

        Triplet compteurs = new Triplet(m, n, o);
        List<String> tmp = chemin(compteurs);
        StringBuilder res = new StringBuilder(
                "Un chemin passant par " + m + " maternités, " + n + " centres de nutrition, et " + o
                        + " blocs opératoirs :\n");
        for (String sommet : tmp) {
            res.append(sommet).append(", ");
        }
        return res.toString();
    }
}
