package GrapheMaillons;

import java.util.ArrayList;
import java.util.List;

/* 
 * La classe ObjetRec est une classe créée spécialement pour la fonction récursive de la classe LCGraphe (rec_chemin()).
 * Elle permet de manipuler une List de List de String, une List de String, un Triplet, un String et un boolean en même temps.
 */

public class ObjetRec {
    private final List<List<String>> tableau;
    private final List<String> chemin;
    private Triplet compteurs;
    private final String racine;
    private boolean racinedejavu;

    /*
     * Constructeur de la classe
     */
    public ObjetRec(List<List<String>> tableau, List<String> chemin, Triplet compteurs, String racine) {
        this.tableau = tableau;
        this.chemin = chemin;
        this.compteurs = compteurs;
        this.racine = racine;
        this.racinedejavu = false;
    }

    /*
     * Renvois la Liste de String
     */
    public List<String> getChemin() {
        return chemin;
    }

    /*
     * Renvois le Triplet
     */
    public Triplet getCompteurs() {
        return compteurs;
    }

    /*
     * Initialise le Triplet
     */
    public void setCompteurs(Triplet compteurs) {
        this.compteurs = compteurs;
    }

    /*
     * Ajoute un String à la List de String
     */
    public void addsommet(String nomSommet) {
        this.chemin.add(nomSommet);
    }

    /*
     * Enlève un String à la List de String
     */
    public void rmvsommet(String nomSommet) {
        this.chemin.remove(nomSommet);
    }

    /*
     * Copie la List de String dans la List de List de String
     */
    public void addchemin(List<String> chemin) {
        List<String> tmp = new ArrayList<>(chemin);
        this.tableau.add(tmp);
    }

    /*
     * Renvois le String
     */
    public String getRacine() {
        return racine;
    }

    /*
     * Renvois le boolean
     */

    public boolean isRacinedejavu() {
        return racinedejavu;
    }

    /*
     * Initialise le boolean
     */
    public void setRacinedejavu(boolean racinedejavu) {
        this.racinedejavu = racinedejavu;
    }

    /*
     * Affiche toutes les données de l'objet de la classe
     */
    @Override
    public String toString() {
        return "ObjetRec [tableau=" + tableau + ", chemin=" + chemin + ", compteurs=" + compteurs + ", racine=" + racine
                + "]";
    }

}