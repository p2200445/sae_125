package Test;
import CSV.ChargementGraphe;
import CSV.ListeSommets;
import CSV.ListeTrajets;
import Exceptions.ExceptionDispencereExistePas;
import GrapheMaillons.LCGraphe;

import java.io.File;
import java.util.Arrays;

public class testChargement {
    public static void main(String[] args) {
        ListeSommets S = new ListeSommets();
        ListeTrajets T = new ListeTrajets();
        File f1 = new File("ListeAdjacenceJeuEssai.csv");
        File f2 = new File("ListeSuccesseursJeuEssai.csv");
        ChargementGraphe.loadingGraph(f1, f2, S, T);
        T.supprDoublons();
        System.out.println(S.afficheTousLesSommets());
        System.out.println(T.afficheTousLesTrajets());
        System.out.println(S.afficheTousLesType("O"));
        System.out.println("Nombre d'arrêtes = "+T.getSize());
        System.out.println(S.afficheNombreType());
        System.out.println(Arrays.deepToString(S.matriceGrapheBoolean()));
        LCGraphe g = ChargementGraphe.loadingLCGraph(S, T);
        try {
            System.out.println(g.cheminPlusCourt("S1", "S20"));
        } catch (ExceptionDispencereExistePas e) {
            throw new RuntimeException(e);
        }
    }
    }