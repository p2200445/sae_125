package Test;
import Exceptions.ExceptionDispencereExistePas;
import Exceptions.ExceptionTousNul;
import Exceptions.ExceptionTypeExistePas;
import Exceptions.ExceptionTypeNegatif;
import GrapheMaillons.*;

import java.util.HashMap;
import java.util.Map;

public class testF10F20 {
    public static void main(String[] args) {
        LCGraphe graphetest = new LCGraphe();

        graphetest.addMain("S1", "M");
        graphetest.addMain("S2", "M");
        graphetest.addMain("S3", "N");
        graphetest.addMain("S4", "N");
        graphetest.addMain("S5", "O");
        graphetest.addMain("S6", "O");

        graphetest.addarrete("S1", "S2", 4, 10, 8);
        graphetest.addarrete("S1", "S4", 5, 8, 18);
        graphetest.addarrete("S2", "S3", 2, 16, 14);
        graphetest.addarrete("S3", "S6", 3, 6, 9);
        graphetest.addarrete("S5", "S4", 1, 20, 6);
        graphetest.addarrete("S6", "S5", 6, 7, 4);

        Map<String, Couple> mapp1 = new HashMap<>();
        Map<String, Couple> mapp2 = new HashMap<>();
        Map<String, Couple> mapp3 = new HashMap<>();
        mapp2.put("S1", new Couple(null, 1));
        mapp3.put("S1", new Couple(null, 1));
        mapp3.put("S4", new Couple(null, 3));

        Triplet cpt = new Triplet(1, 1, 1);

                        System.out.print("\ntest F10\n");
                        System.out.print(graphetest.listevoisinsspecifique2sommets("S1", "S3", "M") +
                        "\n");
                        System.out.print(graphetest.listevoisinsspecifique2sommets("S4", "S4", "O") +
                       "\n");

         // test F11
        System.out.print("\ntest F11\n");
         try {
             System.out.print(graphetest.deuxdisantce("S1", "S5") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }

         try {
             System.out.print(graphetest.deuxdisantce("S2", "S3") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }

         try {
             System.out.print(graphetest.deuxdisantce("S6", "S6") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }


         //test F12
        System.out.print("\ntest F12\n");
         try {
             System.out.print(graphetest.compartdeuxville("S2", "S5") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }

         try {
             System.out.print(graphetest.compartdeuxville("S5", "S5") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }

         System.out.print(graphetest.estVoisin("S1", "S5") + "\n");
         System.out.print(graphetest.estVoisin("S2", "S3") + "\n");
         System.out.print(graphetest.estVoisin("S6", "S6") + "\n");

         //test F13
        System.out.print("\ntest F13\n");
         System.out.print(graphetest.getCoup("S1", "S5", 1) + "\n");
         System.out.print(graphetest.getCoup("S2", "S3", 1) + "\n");
         System.out.print(graphetest.getCoup("S6", "S6", 1) + "\n\n");

         System.out.print(graphetest.getCoup("S1", "S5", 2) + "\n");
         System.out.print(graphetest.getCoup("S2", "S3", 2) + "\n");
         System.out.print(graphetest.getCoup("S6", "S6", 2) + "\n\n");

         System.out.print(graphetest.getCoup("S1", "S5", 3) + "\n");
         System.out.print(graphetest.getCoup("S2", "S3", 3) + "\n");
         System.out.print(graphetest.getCoup("S6", "S6", 3) + "\n\n");

         System.out.print(graphetest.plusgrand(mapp1) + "\n");
         System.out.print(graphetest.plusgrand(mapp2) + "\n");
         System.out.print(graphetest.plusgrand(mapp3) + "\n\n");

         try {
             System.out.print(graphetest.cheminPlusFiable("S4", "S6") + "\n\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }
         try {
             System.out.print(graphetest.cheminPlusFiable("S3", "S3") + "\n\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }


         //test F15
        System.out.print("\ntest F15\n");
         System.out.print(graphetest.pluspetit(mapp1) + "\n");
         System.out.print(graphetest.pluspetit(mapp2) + "\n");
         System.out.print(graphetest.pluspetit(mapp3) + "\n\n");

         System.out.print(graphetest.contient("S3") + "\n");
         System.out.print(graphetest.contient("S8") + "\n");

         try {
             System.out.print(graphetest.cheminPlusRapide("S4", "S6") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }
         try {
             System.out.print(graphetest.cheminPlusRapide("S3", "S3") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }

         try {
             System.out.print(graphetest.cheminPlusCourt("S4", "S6") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }
         try {
             System.out.print(graphetest.cheminPlusCourt("S3", "S3") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {
             System.out.print("Error : " + pbm + "\n");
         }


         //test F19

        System.out.print("\ntest F19\n");
         System.out.print(graphetest.typecompa("S1", new Triplet(1, 1, 1)) + "\n");
         System.out.print(graphetest.typecompa("S1", new Triplet(0, 0, 0)) + "\n");
         System.out.print(graphetest.typecompa("S9", new Triplet(1, 1, 1)) + "\n\n");
         try {System.out.print(graphetest.getType("S4"));
         } catch (ExceptionDispencereExistePas pbm) {System.out.print("Error : " + pbm + "\n");
         }
         try {System.out.print(graphetest.getType("S40"));
         } catch (ExceptionDispencereExistePas pbm) {System.out.print("Error : " + pbm + "\n\n");
         }
         try {cpt = graphetest.modifCompteur("M", cpt, false);System.out.print("compteur de m " + cpt.getM() + ",compteur de n " + cpt.getN() + ", compteur de o " + cpt.getO() + "\n");
         } catch (ExceptionTypeExistePas pbm) {System.out.print("Error : " + pbm + "\n");
         }
         try {cpt = graphetest.modifCompteur("M", cpt, true);System.out.print("compteur de m " + cpt.getM() + ",compteur de n " + cpt.getN() + ", compteur de o " + cpt.getO() + "\n\n");
         } catch (ExceptionTypeExistePas pbm) {System.out.print("Error : " + pbm + "\n\n");
         }
         try {System.out.print(graphetest.getMaillon("S5") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {System.out.print("Error : " + pbm + "\n");
         }
         try {System.out.print(graphetest.getMaillon("S8") + "\n");
         } catch (ExceptionDispencereExistePas pbm) {System.out.print("Error : " + pbm + "\n\n");
         }
        System.out.print(graphetest.cheminTraversantChaqueType() + "\n\n");

         //test F20

        System.out.print("\ntest F20\n");
         try {System.out.print(graphetest.cheminTraversantNSommetDeChaqueType(2, 2, 2));
         } catch (ExceptionTousNul | ExceptionTypeNegatif pbm) {System.out.print("Error : " + pbm + "\n\n");
         }
    }
}
