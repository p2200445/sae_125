package CSV;

public class Trajet {
    public static final String delimiter = ",";
    private double fiab;
    private double dist;
    private double dur;
    private Sommet dest;
    private Sommet ori;

    //Constructors
    public Trajet(String trajet){
        String[] tempArr;
        tempArr = trajet.split(delimiter);
        this.fiab = Integer.parseInt(tempArr[0]);
        this.dist = Integer.parseInt(tempArr[1]);
        this.dur = Integer.parseInt(tempArr[2]);
    }

    // Getters
    public double getFiab() {
        return fiab*10;
    }
    public double getDist() {
        return dist;
    }
    public double getDur() {
        return dur;
    }
    public Sommet getDest() {
        return dest;
    }
    public Sommet getOri() {
        return ori;
    }
    public int getDestNbr(){
        return this.dest.getNumSommet();
    }
    public int getOriNbr(){
        return this.ori.getNumSommet();
    }

    // Adders
    public void addDest(Sommet dest){
        this.dest = dest;
    }
    public void addOri(Sommet ori){
        this.ori = ori;
    }

    // Setters
    public void setFiab(double pourcentage){
        this.fiab = pourcentage/10;
    }
    public void setDist(double dist) {
        this.dist = dist;
    }
    public void setDur(double dur) {
        this.dur = dur;
    }
    @Override
    public String toString() {
        return "{" +
                "Fiabilité = " + fiab*10 +"%"+
                ", Distance = " + dist +"km"+
                ", Durée = " + dur +"min "+
                ", Sommets reliés = "+ ori.getNumSommet()+
                ", "+ dest.getNumSommet()+
                '}';
    }
}
