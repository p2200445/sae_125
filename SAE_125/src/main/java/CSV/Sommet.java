package CSV;

import java.util.List;

public class Sommet {
    private final int numSommet;
    private String typeSommet;
    private final ListeTrajets trajets;

    // Constructors
    public Sommet(String newNom, String newType, ListeTrajets newTraj){
        this.numSommet = Integer.parseInt(newNom);
        this.typeSommet = newType;
        this.trajets = newTraj;
    }
    public void setTypeSommet(String typeSommet){
        this.typeSommet = typeSommet;
    }
    public void setTypeSommetString(String stringType){
        switch (stringType){
            case "Bloc Opératoire" : setTypeSommet("O");
            case "Maternité" : setTypeSommet("M");
            case "Centre de nutrition" : setTypeSommet("N");
        }
    }
    // Getters
    public String getTypeSommet(){
        return this.typeSommet;
    }
    public String getTypeSommetString() {
        return switch (this.typeSommet) {
            case "O" -> "Bloc Opératoire";
            case "M" -> "Maternité";
            case "N" -> "Centre de nutrition";
            default -> null;
        };
    }

    public List<Integer> getAdjTypeList(String type){return this.trajets.listeTrajIntType(type);}
    public List<Integer> getAdj(){
        return this.trajets.listeTrajInt();
    } // F7
    public ListeTrajets getTrajet(){
        return this.trajets;
    }
    public int getNumSommet(){
        return this.numSommet;
    }
    public String getNomSommet(){
        return "S"+this.numSommet;
    }
    @Override
    public String toString() {
        return "Sommet " + numSommet +" {" +
                "   Type = " + getTypeSommetString()+
                "   Sommets adjacents : " + trajets.listeTrajInt() +
                "}";
    }
}