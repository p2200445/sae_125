package CSV;

import java.util.ArrayList;
import java.util.List;

public class ListeTrajets {
        private final List<Trajet> trajetList;
        public ListeTrajets(){
                this.trajetList = new ArrayList<>();
        }
        public List<Trajet> getListeTrajet(){
                return this.trajetList;
        }
        public void ajTraj(Trajet traj){
                if (traj != null) {
                        this.trajetList.add(traj);
                }
        } // Ajoute un trajet
        public boolean checkDoublons(Trajet a, Trajet b){
                return a.getOriNbr() == b.getDestNbr() && a.getDestNbr() == b.getOriNbr();
        } // Vérifie si deux trajets sont égaux
        public void supprDoublons(){
                for (int i = 0; i < this.trajetList.size(); i++){
                        Trajet traj1 = this.trajetList.get(i);
                        for (int y = i+1; y < this.trajetList.size(); y++){
                                Trajet traj2 = this.trajetList.get(y);
                                if (checkDoublons(traj1, traj2)){
                                        supprTraj(traj2);
                                }
                        }
                }
        }
        public void supprTraj(Trajet traj){
                if (traj != null){
                        this.trajetList.remove(traj);
                }
        } // Supprime un trajet
        public Trajet getTraj(int indice){
               return this.trajetList.get(indice);
        } //Getter

        public int getSize(){
                return this.trajetList.size();
        } // Get taille liste
        public List<Integer> listeTrajInt(){
                List<Integer> Adj = new ArrayList<>();
                for (Trajet t : this.trajetList){
                        Adj.add(t.getDestNbr());
                }
                return Adj;
        } //Transforme la liste en liste d'entiers
        public List<Integer> listeTrajIntType(String type){
                List<Integer> destType = new ArrayList<>();
                for (Trajet t : trajetList){
                        if (t.getDest().getTypeSommet().equals(type)){
                                destType.add(t.getDestNbr());
                        }
                }
                return destType;
        } // Transforme la liste en liste d'entiers d'un type donné
        public List<Integer> deuxDistTraj(){
                List<Integer> deuxDist = new ArrayList<>();
                for (Trajet t : this.trajetList){
                        Sommet temp = t.getDest();
                        deuxDist.addAll(temp.getAdj());
                }
                return deuxDist;
        } // Retourne la liste en entiers des adjacents des sommets de destination de chaque trajet, en gros, les sommets à deux distances du sommet d'origine
        public StringBuilder afficherTouteLesDest(){
                StringBuilder adj = new StringBuilder();
                for (Trajet t : this.trajetList){
                        adj.append("|S").append(t.getDestNbr());
                }
                return adj;
        } // ToString Destinations

        public StringBuilder afficheTousLesTrajets(){
                StringBuilder string = new StringBuilder();
                int i = 0;
                for (Trajet trajet : this.trajetList) {
                        i++;
                        string.append("\n");
                        string.append("Trajet ").append(i);
                        string.append(trajet);
                }
                return string;
        } // ToString
        public Object[][] trajetsTab(){
                Object[][] tabData = new Object[this.trajetList.size()][5];
                int y = 1;
                for (int i = 0; i < this.trajetList.size(); i++){
                        Trajet temp = this.trajetList.get(i);
                        tabData[i][0] = "Trajet "+y;
                        tabData[i][1] = temp.getFiab();
                        tabData[i][2] = temp.getDist();
                        tabData[i][3] = temp.getDur();
                        tabData[i][4] = "S"+temp.getOri().getNumSommet()+" | S"+temp.getDest().getNumSommet();
                        y++;
                }
                return tabData;
        }
}
