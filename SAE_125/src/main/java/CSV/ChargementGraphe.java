package CSV;

import GrapheMaillons.LCGraphe;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;

public class ChargementGraphe {
        private static final String delimiter = ";";
         /**
     * loadingGraph(file1, file2, listeS, listeT)
     * <p>
     * file1 File - nom du fichier des adjacents
     * file2 File - nom du fichier des successeurs
     * listeS ListeSommet - liste qui va contenir les informations de chaque sommet du graphe
     * listeT ListeTrajet - liste qui va contenir les informations de chaque trajet du graphe
     * <p>
          *
     * <p>
          *         Cette fonction est en deux parties, une pour chaque fichier.
          *         Dans la première partie, nous utilisons tout d'abord un BufferedReader
          *         afin de lire le fichier ligne par ligne. Par la suite, pour chaque ligne
          *         lue, nous divisons le String en un tableau de String avec la fonction split().
          *         Nous récupérons donc par le suite toute les informations de chaque sommet et
          *         pouvons l'implémenter dans la liste. Dans la deuxieme partie, nous chargeons le
          *         second fichier, ce fichier nous permet de trouver les origines et destinations de
          *         chaque trajet que nous pouvons implémenter avec la fonction addOri() et addDest()
          *         de la classe trajet.
          *
     */
        public static void loadingGraph(File file1, File file2, ListeSommets listeS, ListeTrajets listeT) {
            try {
                FileReader fr = new FileReader(file1);
                BufferedReader bufferedReader = new BufferedReader(fr);
                String line;
                String[] tempArr;
                String nomSommet = "";
                String typeSommet = "";
                while ((line = bufferedReader.readLine()) != null) {
                    ListeTrajets listeTrajTemp = new ListeTrajets();
                    if (!line.contains("//")) {
                        tempArr = line.split(delimiter);
                        for (String tempStr : tempArr) {
                            switch (tempStr) {
                                case "O", "M", "N" -> typeSommet = tempStr;
                                default -> {
                                    if (tempStr.contains("S")) {
                                        nomSommet = tempStr.replaceAll("S", "");
                                    } else if (!tempStr.equals("0")) {
                                        Trajet traj = new Trajet(tempStr.replaceAll("\\s", ""));
                                        Trajet trajbis = new Trajet(tempStr.replaceAll("\\s", ""));
                                        listeTrajTemp.ajTraj(traj);
                                        listeT.ajTraj(trajbis);
                                    }
                                }
                            }

                        }
                        Sommet som = new Sommet(nomSommet, typeSommet, listeTrajTemp);
                        listeS.ajSommet(som);
                    }
                }
                System.out.println();
                bufferedReader.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            // FICHIER 2
            try {
                FileReader fr = new FileReader(file2);
                BufferedReader bufferedReader = new BufferedReader(fr);
                String line2;
                String[] tempArr2;
                int compteurBis = 0;
                while ((line2 = bufferedReader.readLine()) != null) {
                    String numsommet;
                    String numsommetbis;
                    String indice;
                    Sommet ori = null;
                    Sommet dest;
                    tempArr2 = line2.split(delimiter);
                    int compteur = 0;
                    int i = 0;
                    for (String tempStr : tempArr2) {
                        if (tempStr.contains("S")) {
                            numsommet = tempStr.replaceAll("[^\\p{Print}]", "");
                            numsommetbis = numsommet.replaceAll("S", "");
                            ori = listeS.getSommetbyID(Integer.parseInt(numsommetbis));
                        } else if (!tempStr.equals("")) {
                            i++;
                            indice = tempArr2[i];
                            dest = listeS.getSommetbyID(Integer.parseInt(indice));
                            Objects.requireNonNull(ori).getTrajet().getTraj(compteur).addOri(ori);
                            ori.getTrajet().getTraj(compteur).addDest(dest);
                            listeT.getTraj(compteurBis).addOri(ori);
                            listeT.getTraj(compteurBis).addDest(dest);
                            compteur++;
                            compteurBis++;
                        }
                    }
                }
                System.out.println();
                bufferedReader.close();
                listeT.supprDoublons();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
         /**
     * loadingLCGraph(sommets, trajets)
     * <p>
     * sommets ListeSommets - Une Liste d'objets de la classe Sommet
     * trajets ListeTrajets - Une Liste d'objets de la classe trajet
     *
     * @return Une liste simplement chaînée de liste simplement chaînée
     * <p>
     *         Cette fonction transforme la liste de sommets et de trajets que l'on a vu auparavent en
          *         une liste chaînée de liste chainée afin de pouvoir implémenter les algorithmes
          *         de plus courts chemins dans des graphes et donc faire des itinéraires.
     */
        public static LCGraphe loadingLCGraph(ListeSommets sommets, ListeTrajets trajets){
        LCGraphe g = new LCGraphe();
        for (int i = 1; i < sommets.getSize()+1; i++){
            Sommet temp = sommets.getSommetbyID(i);
            g.addMain(temp.getNomSommet(), temp.getTypeSommet());
        }
        for (int y = 0; y < trajets.getSize(); y++){
            Trajet temp = trajets.getTraj(y);
            g.addarrete(temp.getOri().getNomSommet(), temp.getDest().getNomSommet(), temp.getFiab(), temp.getDist(), temp.getDur());
        }
        return g;
    }
}
