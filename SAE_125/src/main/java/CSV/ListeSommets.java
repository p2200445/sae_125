package CSV;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ListeSommets {
        private final List<Sommet> listeSommets;
        private static int nbrO = 0;
        private static int nbrM = 0;
        private static int nbrN = 0;
        public ListeSommets(){
            this.listeSommets = new ArrayList<>();
        }
        public void ajSommet(Sommet ajSom){
            switch (ajSom.getTypeSommet()) {
                case "O" -> nbrO++;
                case "M" -> nbrM++;
                case "N" -> nbrN++;
                default -> {
                }
            }
            this.listeSommets.add(ajSom); // Ajoute un sommet à la liste de sommets
        } // Ajoute un sommet à la liste
        public static int getNbrM() {
            return nbrM;
        } // Get Nombre de Maternité
        public static int getNbrN() {
            return nbrN;
        } // Get Nombre de Centre de Nutritions
        public static int getNbrO() {
            return nbrO;
        } // Get Nombre de Bloc opératoires
        public Sommet getSommetbyID(int indice){
            Sommet som = null;
            for (Sommet s : this.listeSommets) {
                if (indice == s.getNumSommet()) {
                    som = s;
                }
            }
            return som;
        } // Get sommet à partir du numéro après le S
        public Sommet getSommetbyString(String nom){
            Sommet som = null;
            for (Sommet s : this.listeSommets){
                if (s.getNomSommet().equals(nom)){
                    som = s;
                }
            }
            return som;
        } // Get sommet à partir de son nom (S1, S2, S3...)

    // Fonctionnalités

        // F10
        public List<Integer> listeVoisinsSpecifiqueDeuxSommets(Sommet s1, Sommet s2, String type){
            List<Integer> listeVoisins = new ArrayList<>();
            for (Integer i : s1.getAdjTypeList(type)){
                if (i != s2.getNumSommet()) {
                    for (Integer y : s2.getAdjTypeList(type)) {
                        if (Objects.equals(i, y) && y != s1.getNumSommet()) {
                            listeVoisins.add(y);
                        }
                    }
                }
            }
            return listeVoisins;
        }

        // F11
        public boolean deuxDistance(Sommet s1, Sommet s2){
            boolean isDeuxDist = false;
            if (s1.getAdj().contains(s2.getNumSommet())){
                return false;
            }
            if (s1.getTrajet().deuxDistTraj().contains(s2.getNumSommet())){
                    isDeuxDist = true;
            }
            return isDeuxDist;
        }
        public String compartVille(Sommet s1){
            int nbrOS1 = 0;
            int nbrMS1 = 0;
            int nbrNS1 = 0;
            for (Integer i : s1.getTrajet().deuxDistTraj()){
                switch (getSommetbyID(i).getTypeSommet()){
                    case "O":nbrOS1++;
                    case "N":nbrNS1++;
                    case "M":nbrMS1++;
                }
            }
            return"Il y a " + nbrMS1 + " maternités à plus de 2 de distance de " + s1.getNomSommet() + ". \n" +
                    "Il y a " + nbrOS1 + " bloques opératoires à plus de 2 de distance de " + s1.getNomSommet() + ". \n" +
                    "Il y a " + nbrNS1 + " centres de nutritions à plus de 2 de distance de " + s1.getNomSommet() + ". \n";
        } // F12
        public int getSize(){
            return this.listeSommets.size();
        } // Get la taille de la liste
        public int[][] matriceGrapheBoolean(){
            int[][] matrice = new int[listeSommets.size()][listeSommets.size()];
            int i = 0;
            for (Sommet s : listeSommets){
                List<Integer> A = s.getAdj();
                for (int y = 0; y < listeSommets.size(); y++){
                    int isAdj = checkAdja(A, y);
                    matrice[i][y] = isAdj;
                }
                i++;
            }
            return matrice;
        }
        public int checkAdja(List<Integer> A, int y){
            int isAdja = 0;
            for (Integer a : A){
                if (a == y) {
                    isAdja = 1;
                    break;
                }
            }
            return isAdja;
        }
        public Object[][] sommetsTab(){
            Object[][] tabData = new Object[this.listeSommets.size()][3];
            for(int i = 0; i < this.listeSommets.size(); i++){
                Sommet temp = this.listeSommets.get(i);
                tabData[i][0] = "S"+temp.getNumSommet();
                tabData[i][1] = temp.getTypeSommetString();
                tabData[i][2] = temp.getTrajet().afficherTouteLesDest();
            }
            return tabData;
        } // Transforme la liste de sommets en un tableau en deux dimensions pour l'afficher dans PanelSommetsArretes
        public StringBuilder afficheNombreType(){
            StringBuilder string = new StringBuilder();
            string.append("\n Nombre de Bloc Opératoire : ").append(getNbrO());
            string.append("\n Nombre de Maternité : ").append(getNbrM());
            string.append("\n Nombre de Centre de Nutrition : ").append(getNbrN());
            return string;
        } // ToString du nombres de dispensaires pour chaque type
        public StringBuilder afficheTousLesSommets(){
            StringBuilder string = new StringBuilder();
            for (Sommet sommet : this.listeSommets) {
                string.append(sommet).append("\n");
            }
            if (this.listeSommets.size() < 1){
                string.append("Il n'y a rien");
            }
            return string;
        } // ToString
        public StringBuilder afficheTousLesType(String Type){
            StringBuilder string = new StringBuilder();
            for (Sommet sommet: this.listeSommets){
                if (sommet.getTypeSommet().equals(Type)){
                    string.append(sommet).append("\n");
                }
            }
            return string;
        } //Affiche tout les sommets d'un type donné
    }

